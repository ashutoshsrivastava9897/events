<?php  
    require_once 'lib/header.php';
?>

<!--==========================
      About Us Section
    ============================-->
    <section id="about">

<div class="container">
    <div class="row">

        <div class="col-lg-5 col-md-6">
            <div class="about-img">
                <img src="img/about-img.jpg" alt="">
            </div>
        </div>

        <div class="col-lg-7 col-md-6">
            <div class="about-content">
                <h2>About Us</h2>
                <h3>Our philosophy.</h3>
                <p>Event Planner Ltd is an event logistics and marketing company which was formed back in 2013. The company offers A-Z event planning services from a team of experienced and energetic event planners, suppliers, venues and more. One of the main reasons behind the success of Event Planner is the fact that the team does not charge fees to its clients! With the number of events we organise, Event Planner Ltd does not need to add exorbitant fees and mark-ups to make its profit margins. This ensures that our clients list, which is constantly growing, make regular use of our services.

There is no fee. There is no mark-up!</p>
                <p>



The Event Planner team does not charge any fees to its clients*. Yes, this might sound strange. However, the company earns its profits through its suppliers, with whom a very strong relationship has been built. This does not mean that the costs are up-marked to make up for the fee. On the contrary, they are less than it would cost a company when booking directly. We know it sounds too good to believe, but over the years this formula has helped the Event Planner team become the success story they are today. This has not in any way reduced the level of professionalism with which the services are carried, as shown by the large number of local and international clients,  who work with us on a regular basis.

In a nutshell, you pay no fee, pay your supplier less and have a professional team handling all your loose ends. Sounds too good, but it is true! You can check out our clients as testimonials</p>
                <ul>
                    <li><i class="ion-android-checkmark-circle"></i>  A-Z event planning. We shall work on your event from beginning to end..</li>
                    <li><i class="ion-android-checkmark-circle"></i> You may relax and leave all the logistics to us.at</li>
                </ul>
            </div>
        </div>
    </div>
</div>

</section>

<?php
        require_once 'lib/footer.php';
        ?>
        <?php
        require_once 'lib/js-links.php';
        ?>
