<?php  
    require_once 'core.php';
    ?>
    <?php
session_start();
if(isset($_POST["username"]) && isset($_POST["password"])){
    $result = $conn->query("select username,upassword from users");
    if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
        if($row["username"]==$_POST["username"] && $row["upassword"]==$_POST["password"]){
            echo 'loged in';
            $_SESSION['login_user'] = $_POST["username"];
            header('location:viewer');
        }
        else{?>
        <p style="position: absolute;top: 44px;right: 219px;"><?php  echo 'failed to login';?></p>
            <?php
        
        }
        }
    }
}
else if(isset($_POST["new_username"]) && isset($_POST["new_password"]) && isset($_POST["phone"])){   
    $newuser=$_POST["new_username"];
    $pass = $_POST["new_password"];
    $phone = $_POST["phone"];
    $entry= "INSERT INTO `users`(username, upassword,phone) VALUES('$newuser','$pass','$phone');";
    if ($conn->query($entry) === TRUE) {
        $_SESSION['login_user']= $newuser;
        $create_tab = "create table $newuser(id int primary key auto_increment,event_title text,date text,venue text,time timestamp,othernotes text default '  ',members text default '  ',event_status text default 'pending');";
        if ($conn->query($create_tab) === TRUE){
        echo "created";
        }
        else{
        echo $conn->error;}
        header('location:viewer');
    } else {?>
    <p><?php echo 'Sorry! Wrong credentials'; ?></p>
     <?php
    }
}


if(isset($_POST["notes"]) && isset($_POST["member"])){
    $eventid = (int)$_POST["eventid"];
    $title = $_POST["title"];
    $date = $_POST["date"];
    $venue = $_POST["venue"];
    $member = $_POST["member"];
    $notes = $_POST["notes"];
    $user = $_SESSION['login_user'];
    $sql = "UPDATE `$user` SET `venue` = '$venue', `date` = '$date', `othernotes` = '$notes', `members` = '$member', `event_title` = '$title' WHERE `$user`.`id` = $eventid;";
    if($conn->query($sql) === true){
        header('location:viewer');
}
    else{
        echo $sql;
        echo $conn->error;
    }
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Es-Rise Events</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="" name="keywords">
    <meta content="" name="description">

    <!-- Favicons -->
    <link href="img/favicon.png" rel="icon">
    <link href="img/apple-touch-icon.png" rel="apple-touch-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,500,600,700,700i|Montserrat:300,400,500,600,700" rel="stylesheet">

    <!-- Bootstrap CSS File -->
    <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Libraries CSS Files -->
    <link href="lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="lib/animate/animate.min.css" rel="stylesheet">
    <link href="lib/ionicons/css/ionicons.min.css" rel="stylesheet">
    <link href="lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
    <link href="lib/lightbox/css/lightbox.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">

    <!-- Main Stylesheet File -->
    <link href="css/style.css" rel="stylesheet">

    <!-- =======================================================
    Theme Name: Rapid
    Theme URL: https://bootstrapmade.com/rapid-multipurpose-bootstrap-business-template/
    Author: BootstrapMade.com
    License: https://bootstrapmade.com/license/
  ======================================================= -->
</head>

<body>
    <!--==========================
  Header
  ============================-->
    <header id="header">

        <div id="topbar">
            <div class="container">
                <div class="social-links">
                    <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
                    <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
                    <a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a>
                    <a href="#" class="instagram"><i class="fa fa-instagram"></i></a>
                </div>
            </div>
        </div>

        <div class="container">

            <div class="logo float-left">
                <!-- Uncomment below if you prefer to use an image logo -->
                <!-- <h1 class="text-light"><a href="#intro" class="scrollto"><span>Rapid</span></a></h1> -->
                <a href="index" class="scrollto"><img src="img/logo.png" alt="" class="img-fluid"></a>
            </div>

            <nav class="main-nav float-right d-none d-lg-block">
                <ul>
                    <li class="active"><a href="index">Home</a></li>
                    <li><a href="about">About Us</a></li>
                    <li><a href="services">Services</a></li>
                    <li><a href="#footer">Contact Us</a></li>
                </ul>
            </nav>
            <!-- .main-nav -->

        </div>
    </header>

    