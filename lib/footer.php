<footer id="footer" class="section-bg">
        <div class="footer-top">
            <div class="container">

                <div class="row">

                    <div class="col-lg-6">

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="footer-links">
                                    <h4>Contact Us</h4>
                                    <p>
                                        A108 Adam Street <br> New York, NY 535022<br> United States <br>
                                        <a href="tel:8126134565"><strong>Phone:</strong> +91 8126134565<br></a>
                                        <a href="mailto:ashutoshsrivastava9897@gmail.com"><strong>Email:</strong> ashutoshsrivastava9897@gmail.com<br></a>
                                    </p>
                                </div>

                                <div class="social-links">
                                    <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
                                    <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
                                    <a href="#" class="instagram"><i class="fa fa-instagram"></i></a>
                                    <a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a>
                                </div>
                                </div>

                                

                            </div>

                        </div>

                    </div>
                </div>

            </div>
        </div>

    </footer>